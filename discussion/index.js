//ES6 Updates
console.log(Math.pow(7,3));
console.log(7 ** 3);

//Template Literals
let from = 'sir Marts'
let message = 'Hello Batch 145 love ' + from;
console.log(message);

//how to create multi-line strings?
console.log('Hi sirs and mam \nHello sirs and mam');

//Using Backticks
console.log(`Hello Batch 145 love, ${from}`);
console.log(`
hello
hello
hello
	`);

//with embedded JS expressions
const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal * interestRate}`)

//before
let message2 = 'Hello' + name + "Welcome to programming!";
console.log('message without template literals: ' + message2);

//using template literals
message2 = `Hello ${name}! Welcome to programming!`;
console.log(`message with template literals: ${message2}`);

//[DESTRUCURE ARRAYS AND OBJECTS using ES6 METHODS]
let grades = [89, 87, 78, 96];
let fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);

//Array Destructuring
let [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);



function addAndSubtract(a,b){
	return [a+b, a-b];
}

const array = addAndSubtract(4,2);
console.log(array);

const [sum,difference] = addAndSubtract(4,2);
console.log(sum);
console.log(difference);

const letters = ['A', 'B', 'C', 'D', 'E'];
const [a, ...rest] = letters;
console.log(a);
console.log(rest);

//Destructure Objects
console.warn('Object Destructuring:');

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//before
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello, my name is ${person.givenName} ${person.maidenName} ${person.familyName}!`);

//Object destructuring
const {givenName,maidenName,familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello, my name is ${givenName} ${maidenName} ${familyName}!`);

// function printUser(user){
// 	console.log(user);
// }

function printUser({givenName: nickName,familyName}){
	console.log(`I am ${nickName} ${familyName}`);
}

printUser(person);

const person2 = {
	"given name": "James",
	"family Name": "Bond"
};
console.log(person2['given name']);

const person3 = {
	name2:'Jill',
	age: 27,
	email: 'gibotajada@gmail.com',
	address: {
		city:'Some city',
		street: 'Some street'
	}
};

// const {name, age} = person3;
const {name2, age, email = null} = person3;
console.log(name2);
console.log(age);
console.log(email);



//ARROW FUNCTIONS
console.log('Arrow Functions:');
//Syntax:
//const/let variableName = (parameters) => {statements};
//before
const hello = function(){
	console.log("Hello world!");
}

//Using arrow function
const helloAgain = () => {
	console.log("Hello world!");
}
hello();
helloAgain();

//Implicit Return Statement
console.warn('Arrow Functions: Implicit Return');
// const add = (x,y) => {return x+y};
const add = (x,y) => x + y;
let total = add(1,2);
console.log(total);


//before
function printFullName(firstName,middleInitial,lastName)
{
	console.log(firstName + ' ' + middleInitial + '- ' + lastName);
}

printFullName("John", "D", "Smith");

const printFullName2 = (firstName, middleInitial,lastName)  => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName2("John", "D", "Smith");

//Arrow functions using loops
const students = ["Joy", "Jade", "Judy"];

//before
students.forEach(function(student){
	console.log(`${student} is a student.`);
})

//using arrow function
students.forEach((student) =>{
	console.log(`${student} is a student.`);
})

const numbers = [1,2,3,4,5];
//before
let numberMap = numbers.map(function(number){
	return number * number;
})
console.log(numberMap);

let numberMap2 = numbers.map((number) => {
	return number * number;
})
console.log(numberMap2);


//DEFAULT FUNCTION ARGUMENT
console.warn('Default Function Argument Value:')

const greet = (name = "User") => {
	return `Good morning, ${name}`;
}

console.log(greet("Jake"));
console.log(greet());


//CLASS-BASED OBJECT BLUEPRINTS
console.warn('Class-Based Object Blueprints:');
/*syntax
class className {
	constructor(propertyA, propertyB){
	this.propertyA = propertyA;
	this.propertyB = propertyB;
	}
}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.year = year;
		this.name = name;
	}
}

//creating a new instance of car object
const myCar = new Car();
console.log(myCar);
 //values of properties may be assigned after creation of an object

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptop';
myCar.year = 2021;
console.log(myCar);

//creating new instance of car with initialized values
const myNewCar = new Car('Toyota','Vios', 2021);
console.log(myNewCar);

let fruits = ["mango", "pear", "banana"];
let mappedFruits = fruits.map(fruit => "banana");
console.log(mappedFruits[2]);